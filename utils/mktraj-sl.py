import sys
from math import *
import string
import glob
import shutil
import itertools as it
import copy
import argparse

##============================================================================##
##---------------------------------- getatoms --------------------------------##
##============================================================================##
def getatoms(gfilename, correction, boxown, xyz, types, showbuff):
    with open(gfilename,'r') as fidi:
        #print gfilename
        startatoms = False
        for line in fidi:
            arglist = line.split()
            if len(arglist) < 1: continue
            #print arglist
            if arglist[0] == 'Velocities': break
            if startatoms:
                it = int(arglist[1])
                x  = float(arglist[2])
                y  = float(arglist[3])
                z  = float(arglist[4])

                xouto = (x>=boxown[0][1] or x<boxown[0][0])
                youto = (y>=boxown[1][1] or y<boxown[1][0])
                zouto = (z>=boxown[2][1] or z<boxown[2][0])
                if (xouto or youto or zouto): # Thi atom is outside "owned" region
                    if not showbuff:
                        continue
                    else:
                        xoutb = (x>=(boxown[0][1]+dbuf) or (x<boxown[0][0]-dbuf))
                        youtb = (y>=(boxown[1][1]+dbuf) or (y<boxown[1][0]-dbuf))
                        zoutb = (z>=(boxown[2][1]+dbuf) or (z<boxown[2][0]-dbuf))
                        if (xoutb or youtb or zoutb):
                            it = 3 # Fixed atom
                        else:
                            it = 2 # Buffer atom

                x+=correction[0]; y+=correction[1]; z+=correction[2]
                xyz.append([x,y,z])
                types.append(it)
                #print x, y, z
            if (arglist[0] == "Atom" or arglist[0] == "Atoms") and (arglist[1] == "ID," or arglist[1] == "#"):
                startatoms = True
    return xyz, types

##============================================================================##
##-------------------------------- check_drmax -------------------------------##
##============================================================================##
def check_drmax(xyz, xyzlastin, tol):
    xyzlast = xyzlastin
    checkall = False # SLOW Option to allow multiple atoms to be <tol...
    if not (len(xyz) == len(xyzlast)):
        print 'NOTE: len(xyz)=',len(xyz),'- len(xyzlast)=',len(xyzlast)
        return False
    for i in xrange(len(xyz)):
        dmin = None
        iimin = 0
        for ii in xrange(len(xyzlast)):
            dr2 = 0.0
            for j in xrange(3):
                dx = xyz[i][j] - xyzlast[ii][j]
                dr2 += dx*dx
            dr = sqrt(dr2)
            if (dmin==None or dr<dmin):
                dmin = dr; iimin = ii
                if not checkall:
                    if dmin < tol: break
        if (dmin != None) and (dmin < tol): xyzlast.pop(iimin)
        else:
            #print 'FALSE    dmin =',dmin
            return False
    #print 'TRUE     dmin =',dmin
    return True

##============================================================================##
##---------------------------------- writelmp --------------------------------##
##============================================================================##
def writelmp(outname,simbox,natoms,ntypes,types,xyz):
    with open(outname,'w') as fout:
        [xlo, xhi, ylo, yhi, zlo, zhi] = simbox
        dx = xlo
        dy = ylo
        dz = zlo

        fout.write('  LAMMPS Data file, converted from CLSMAN format\n\n');
        fout.write(\
            '  %f  %f   xlo xhi\n  %f  %f   ylo yhi\n  %f  %f   zlo zhi\n' \
            %(xlo-dx,xhi-dx,ylo-dy,yhi-dy,zlo-dz,zhi-dz))
        fout.write('\n\n')
        fout.write('     %d atom types\n\n' %(int(ntypes)));
        fout.write('     %d atoms\n\n' %(int(natoms)))
        fout.write('Atoms\n   Atom ID, Atom type, X, Y, Z\n')

        #Atom coordinates
        for atomid in xrange(natoms):
            itype = types[atomid]
            [x, y, z] = xyz[atomid]
            fout.write('%5i  %5i  %20.15f  %20.15f  %20.15f\n' %(atomid+1, \
                itype, float(x-dx), float(y-dy), float(z-dz)))
    return

################################################################################
##============================================================================##
##------------------------------------ MAIN ----------------------------------##
##============================================================================##
################################################################################
if __name__ == "__main__":

    # Default Sub-lattice Parameters
    rcheck   = False #True                    # Check for same coords with new label
    tol      = 0.2                     # Tolerance to consider an atom different
    res      = 1                       # Resolution of trajectory
    states   = 'states'                # Directory containing geo files
    tfile    = 'trajSL.out'            # Trajectory file
    gfile    = 'input.lammps.adatom'   # Reference GLOBAL geometry file
    nxyz_in  = '8,8,1'                 # Comma-separated: sub-lattice dimensions
    dbfv_in  = '6.0,6.0,6.0'           # Comma-separated: dbuf, dfix, dvac

    # Parse Command-line Input
    parser = argparse.ArgumentParser()
    parser.add_argument("--showb", dest="showb", action="store_true", default=False)
    parser.add_argument("--readt", dest="readt", action="store_true", default=False)
    parser.add_argument("-r", "--res",    dest="res",    type=int, default=res)
    parser.add_argument("-s", "--states", dest="states", type=str, default=states)
    parser.add_argument("-t", "--tfile",  dest="tfile",  type=str, default=tfile)
    parser.add_argument("-g", "--gfile",  dest="gfile",  type=str, default=gfile)
    parser.add_argument("-n", "--nxyz",   dest="nxyz",   type=str, default=nxyz_in)
    parser.add_argument("-d", "--dbfv",   dest="dbfv",   type=str, default=dbfv_in)
    args = parser.parse_args()
    minPerCycle = args.res
    run = args.states
    tfilename = args.tfile
    gfilename = args.gfile
    nxyz = [int(item)   for item in args.nxyz.split(',')]
    nx, ny, nz = nxyz
    dbfv = [float(item) for item in args.dbfv.split(',')]
    dbuf, dfix, dvac = dbfv
    readtraj = args.readt # Read traj.out
    showbuff = args.showb # Show buffer/fixed atoms for each sld...
    ntraj = nx*ny*nz

    if readtraj:

        # Read contents of 'tfilename' (traj.out):
        inc=open(tfilename,'r')
        i={}
        ind={}
        cl={} # Local state label for this visit
        tl={} # Local time for this visit
        tt={} # Total time when this visit ended
        time={} # scalar for each trajectory (last time appended to tt)
        ntraj=1
        for l in inc.readlines():
            try:
                sl=string.split(l)
                c=int(sl[0])
                t=int(sl[1])
                itraj=int(sl[2])
                #print c, t, itraj
                if itraj in cl:
                    cl[itraj].append(c)
                    tl[itraj].append(t)
                    tt[itraj].append(t+time[itraj])
                    time[itraj] += t
                    ntraj = max(ntraj,itraj+1)
                else:
                    ind[itraj] = 0
                    i[itraj]   = 0
                    cl[itraj]  = [c]
                    tl[itraj]  = [t]
                    tt[itraj]  = [t]
                    time[itraj] = t
            except:
                pass
        inc.close()

    # Read Global Input:
    boxhi = [0.0, 0.0, 0.0]
    boxlo = [0.0, 0.0, 0.0]
    natoms = 0; ntypes = 0
    nxyzSL = [nx, ny, nz]
    with open(gfilename,'r') as fidi:
        startatoms = False
        for line in fidi:
            arglist = line.split()
            #arglist = re.split(' |,',line)
            #print arglist
            if len(arglist) < 1: continue
            if not startatoms:
                if   (len(arglist) > 3) and (arglist[2] == "xlo"):
                    boxlo[0] = float(arglist[0])
                    boxhi[0] = float(arglist[1])
                    #print "boxlo, boxhi [0]: ",boxlo[0],boxhi[0]
                elif (len(arglist) > 3) and (arglist[2] == "ylo"):
                    boxlo[1] = float(arglist[0])
                    boxhi[1] = float(arglist[1])
                    #print "boxlo, boxhi [1]: ",boxlo[1],boxhi[1]
                elif (len(arglist) > 3) and (arglist[2] == "zlo"):
                    boxlo[2] = float(arglist[0])
                    boxhi[2] = float(arglist[1])
                    #print "boxlo, boxhi [2]: ",boxlo[2],boxhi[2]
                elif (len(arglist) > 2) and (arglist[1] == "atom") \
                                        and (arglist[2] == "types"):
                    ntypes = int(arglist[0])
                    #print "ntypes: ",ntypes
                elif (len(arglist) > 1) and (arglist[1] == "atoms"):
                    natoms = int(arglist[0])
                    #print "natoms: ",natoms
            if startatoms: break
            if (arglist[0] == "Atom") and (arglist[1] == "ID,"): startatoms = True


    # General SSD Parameters:
    sld_dims = [0.0,0.0,0.0]
    sld_dims_full = [0.0,0.0,0.0]
    for i in xrange(3):
        sld_dims[i] = float(boxhi[i]-boxlo[i]) / float(nxyzSL[i])
        if(sld_dims[i] < (2.0 * dbuf)): print "WARNING: SLD is too small!!!"
        sld_dims_full[i] = sld_dims[i]
        if (nxyzSL[i]>1): sld_dims_full[i] += 2.0 * (dbuf + dfix + dvac)
    print "SLD Dimensions Are:", sld_dims[0], sld_dims[1], sld_dims[2]
    if showbuff:
        print "FULL SLD Dimensions Are:", sld_dims_full[0], sld_dims_full[1], sld_dims_full[2]


    # Add extra room if we are showing ALL atoms..
    if showbuff:
        ntypes = 3
        for idim in xrange(3):
            # extend simulation box if we are going to show a
            # 'blown-up view'...
            boxhi[idim] = nxyzSL[idim] * sld_dims_full[idim]


    # Determine the corrections to 'local' atoms when writing 'global' file
    corrections = []
    itraj = 0
    adjust = (dbuf+dfix+dvac)
    for kk in xrange(nz):
        cz = sld_dims[2]*kk
        if nz > 1: cz-=adjust
        for jj in xrange(ny):
            cy = sld_dims[1]*jj
            if ny > 1: cy-=adjust
            for ii in xrange(nx):
                cx = sld_dims[0]*ii
                if nx > 1: cx-=adjust
                corrections.append([cx, cy, cz])
                print "itraj",itraj,"correction:", corrections[-1]
                itraj+=1
    boxown = [[0.0, sld_dims[0]],[0.0, sld_dims[1]],[0.0, sld_dims[2]]]
    if nx > 1: boxown[0][0]+=adjust; boxown[0][1]+=adjust
    if ny > 1: boxown[1][0]+=adjust; boxown[1][1]+=adjust
    if nz > 1: boxown[2][0]+=adjust; boxown[2][1]+=adjust
    print "boxown:", boxown

    # Redo the 'corrections' if we are showing everything...
    if showbuff:
        itraj = 0
        for kk in xrange(nz):
            cz = sld_dims_full[2]*kk
            for jj in xrange(ny):
                cy = sld_dims_full[1]*jj
                for ii in xrange(nx):
                    cx = sld_dims_full[0]*ii
                    corrections[itraj] = [cx, cy, cz]
                    print "SHOWBUFF -> itraj",itraj,"correction:", corrections[itraj]
                    itraj+=1

    summaryfile = 'trajSL.out'
    refind = 0
    if readtraj:

        summaryfile = 'traj.sl.summary'
        refind = 1

        # Write out a summary of this global trajectory
        t = 0.0
        last = None
        with open(summaryfile,'w') as fout:
            while True:
                good = False; labels = []
                for itraj in range(ntraj):
                    if not itraj in cl:
                        labels.append(666)
                        continue
                    for j in range(ind[itraj], len(tt[itraj])):
                        if tt[itraj][j] >= t:
                            labels.append(cl[itraj][j])
                            ind[itraj] = j
                            good = True; break
                        elif j == len(tt[itraj])-1: labels.append(cl[itraj][-1])
                if not (labels == last):
                    fout.write('%24i' %(t))
                    for label in labels: fout.write(' %24i' %(label))
                    fout.write('\n')
                last = labels
                t+=minPerCycle
                if (not good): break

    # Loop through our summary file and create a global geometry files:
    ifcnt = 0
    xyzlast = []
    lastline = None
    with open(summaryfile,'r') as fin:
        for line in fin:
            arglist = line.split()
            if len(arglist) < 1: continue

            if (line==lastline): continue # no need to write the same file...

            if readtraj: time = int(arglist[0])

            xyz=[]; types=[]
            for itraj in xrange(ntraj):
                label = int(arglist[refind+itraj])
                filei = run+"/"+"state-%i.out" % (label)
                xyz, types = getatoms(filei, corrections[itraj], boxown, xyz, types, showbuff)

            if rcheck and (len(xyzlast) > 0):
                if check_drmax(xyz, xyzlast, tol):
                    xyzlast = xyz; continue

            fileo=run+"/"+"sltraj-%09d.out" % (ifcnt)
            simbox = [boxlo[0],boxhi[0],boxlo[1],boxhi[1],boxlo[2],boxhi[2]]
            #print 'SIZE: ',len(xyz), len(types)
            #print 'EXPECTED: ',natoms, ntypes
            writelmp(fileo,simbox,len(xyz),ntypes,types,xyz)
            xyzlast = xyz
            lastline = line
            ifcnt+=1
    sys.exit(0)
