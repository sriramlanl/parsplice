FROM ubuntu:latest

#==============================================================================#
#
# File Name:       Dockerfile-ParSpliceDeps
#
# This "Dockerfile" was used by to build the dependencies for ParSplice within
# a generic "ubuntu" image. The resulting image is used to perform the build
# test using the gitlab-ci.yml script in exaalt/parsplice.
#
# Result:          registry.gitlab.com/exaalt/parsplice/ubuntu-parsplice-deps
#
# Original Author: Richard J. Zamora, April 26th 2017
#
# Last Modified:   September 9th 2017
#
# Use Instructions:
#
#    (1) Install Docker (https://www.docker.com/get-docker)
#        --> For Mac, (https://www.docker.com/docker-mac)
#
#    (2) Modify this Dockerfile script (and name it "Dockerfile")
#        --> Set the "USERNAME" and "PASSWORD" environment variables to match
#            your personal gitlab.com account
#
#    (3) In a new directory containing the "Dockerfile", build an image:
#        --> $ docker build -t <image-name> .
#
#        Note: <image-name> is your choice (ex: ubuntu-exaalt)
#
#        Note: At LANL, may need to use the following command instead:
#
#        --> $ docker build --build-arg http_proxy=$http_proxy
#                         --build-arg https_proxy=$https_proxy -t <image-name> .
#
#        --> $ docker image ls
#
#    (*) To build and push an image to gitlab.com for "CI":
#        --> $ docker login registry.gitlab.com
#
#        --> Then, for following use:
#            <full-image-name> = registry.gitlab.com/project-path/<image-name>
#
#        --> $ docker build --build-arg http_proxy=$http_proxy
#                  --build-arg https_proxy=$https_proxy -t <full-image-name> .
#
#        Note: The image can be used in the ".gitlab-ci.yml" file in the
#              exaalt/parsplice/ project to perform tests on a "runner". To do
#              this you would use...
#
#              <full-image-name> =
#                            registry.gitlab.com/exaalt/parsplice/<image-name>
#
#        --> $ docker image ls
#
#        Note: Look at the hash "tag" for this image
#
#        --> $ docker tag <tag-id> <full-image-name>:<tag-name>
#
#        Note: <tag-name> can be some label of choice, ex: "latest"
#
#        --> $ docker push <full-image-name>
#
#==============================================================================#

# Install basic ubuntu functionality:
RUN apt-get update \
  && apt-get --yes install gfortran libblas-dev liblapack-dev \
  && apt-get --yes install cmake python gcc cmake-data g++ git wget \
  && apt-get --yes install pkg-config python-numpy python3-numpy libopenmpi-dev

# Setup environemnt:
ENV HOME /usr/local/
WORKDIR /usr/local/
# USERNAME:     Gitlab.com username
# PASSWORD:     Gitlab.com password
# local_PREFIX: "Local" directory containing dependencies
# super_PREFIX: Directory where apt-get installs boost
ENV USERNAME <your-gitlab-username>
ENV PASSWORD <your-gitlab-password>
ENV local_PREFIX /usr/local/
ENV super_PREFIX /usr/

# Install boost:
RUN apt-get --yes install libboost-all-dev

# Clone exaalt_deps repository:
RUN git clone https://rjzamora@gitlab.com/rjzamora/exaalt_deps.git

# Install bdb:
RUN cd /usr/local/exaalt_deps/ \
  && tar -xvf db-6.2.23.tar \
  && cd /usr/local/exaalt_deps/db-6.2.23 \
  && cd build_unix \
  && ../dist/configure --enable-stl --enable-cxx --prefix=/usr/local/ \
  && make \
  && make install

# Install eigen:
RUN cd /usr/local/exaalt_deps/eigen-eigen-26667be4f70b \
  && cd /usr/local/exaalt_deps/eigen-eigen-26667be4f70b \
  && mkdir /usr/local/exaalt_deps/eigen-eigen-26667be4f70b/build \
  && cd /usr/local/exaalt_deps/eigen-eigen-26667be4f70b/build \
  && cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ ../ \
  && make install

# Install nauty:
RUN cd /usr/local/exaalt_deps/ \
  && tar -xvf nauty26r7.tar \
  && cd /usr/local/exaalt_deps/nauty26r7 \
  && ./configure --prefix=/usr/local/ \
  && make \
  && cp nauty.a /usr/local/lib/libnauty.a \
  && mkdir /usr/local/include/nauty \
  && cp *.h /usr/local/include/nauty

## Build and Install latte:
RUN  git clone https://github.com/lanl/LATTE.git \
  && cd ${HOME}/LATTE \
  && cp ./makefiles/makefile.CHOICES.gfort.lapack.lmp makefile.CHOICES \
  && make
ENV latte_PATH ${HOME}/LATTE

## Build and Install lammps:
RUN  git clone https://${USERNAME}:${PASSWORD}@gitlab.com/exaalt/lammps.git \
  && cd ${HOME}/lammps \
  && git checkout exaalt \
  && git pull origin exaalt \
  && cd ${HOME}/lammps/lib/latte \
  && cp Makefile.lammps.gfortran Makefile.lammps \
  && cd ${HOME}/lammps/src \
  && make yes-latte \
  && make yes-molecule \
  && make yes-manybody \
  && make yes-kspace \
  && make yes-snap \
  && make yes-misc \
  && cp MAKE/MACHINES/Makefile.ubuntu_simple MAKE/Makefile.mpi \
  && make mpi \
  && make mpi mode=lib \
  && cp ${HOME}/lammps/src/liblammps_mpi.a ${local_PREFIX}/lib/. \
  && mkdir ${local_PREFIX}/include/lammps \
  && cp ${HOME}/lammps/src/*.h  ${local_PREFIX}/include/lammps/.

ENV HOME /root

CMD ["/bin/bash"]
