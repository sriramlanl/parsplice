import sys
from math import *
import string
import glob
import shutil
import itertools as it

import copy

run=sys.argv[1]

skipLoops=True

if len(sys.argv)>2:
    skipLoops=bool(int(sys.argv[2]))
else:
    skipLoops=False

#print skipLoops, len(sys.argv), sys.argv[2], bool(sys.argv[2])

inc=open('traj.out','r')

i={}
cl={}
tl={}
ntraj=1
for l in inc.readlines():
    try:
        sl=string.split(l)
        c=int(sl[0])
        t=int(sl[1])
        itraj=int(sl[2])
        if itraj in cl:
            cl[itraj].append(c)
            tl[itraj].append(t)
            ntraj = max(ntraj,itraj+1)
        else:
            i[itraj]  = 0
            cl[itraj] = [c]
            tl[itraj] = [t]
    except:
        pass

#i=0

# Loop through all trajectories:
for itraj in range(ntraj):

    if not itraj in cl: continue

    if skipLoops:
        last={}
        for j in range(len(cl[itraj])):
            last[cl[itraj][j]]=j

        #indices = xrange(len(cl[itraj]) - 1, 0, -1)
        #gen = it.izip(indices, reversed(cl[itraj]))
        #cr=copy.copy(cl[itraj])
        #cr.reverse()
        j=0
        while j<len(cl[itraj]):
            c=cl[itraj][j]
            filei=glob.glob(run+"/"+"state-%i.out" % c)
            #print filei
            if len(filei)>0:
                if ntraj>1: fileo=run+"/"+"traj-%d-%09d.out" % (itraj,i[itraj])
                else:       fileo=run+"/"+"traj-%09d.out" % (i[itraj])
                shutil.copyfile(filei[0],fileo)
                print filei,fileo
                i[itraj]+=1
            else:
                #print "Skipped ", c
                pass

            if not (c==0 or c==666):
                #print c
                jp=j
                j=last[c]
                #print "jumping from ",jp," to ",j
                """
                try:
                    jp=j
                    j=len(cr)-1-cr.index(c)
                    print "jumping from ",jp," to ",j
                except:
                    pass
                """
            j+=1
    else:
        for j in range(len(cl[itraj])):
            c=cl[itraj][j]
            t=tl[itraj][j]
            #print c

            filei=glob.glob(run+"/"+"state-%i.out" % c)
            #print filei
            if len(filei)>0:
                if ntraj>1: fileo=run+"/"+"traj-%d-%09d.out" % (itraj,i[itraj])
                else:       fileo=run+"/"+"traj-%09d.out" % (i[itraj])
                shutil.copyfile(filei[0],fileo)
                print filei,fileo
                i[itraj]+=1
            else:
                #print "Skipped ", c
                pass

