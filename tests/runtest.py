#!/usr/bin/env python2

import sys            # Runs regression tests for the parsplice code
import re             #
import subprocess     # Syntax:
import filecmp        #   python runtest.py <mpi-exe> <exe-root> <test-name>
                      #
                      # Author: R.J. Zamora (rjzamora@lanl.gov)
                      #
                      #     Last modified: November 7, 2017

##============================================================================##
##------------------------------ Helper Functions ----------------------------##
##============================================================================##

# Directly compare file (outfn) to same file name in "gold/"...
def cmp_std(outfn):
    goldfile = "gold/"+outfn
    savefile = outfn+".0"
    match = filecmp.cmp(goldfile,outfn)
    subprocess.call(["mv",outfn,savefile])
    if(match):
        print("FILES MATCH."); return(0)
    else:
        print("FILES DON'T MATCH!!"); return(1)

# Search for strings in a given file (outfn)...
# (Note: Function may be bad for large files)
def find_strs(outfn, str_list):
    for str_item in str_list:
        print("Looking for "+str_item)
        f = open(outfn)
        match = (str_item in f.read())
        if (not match):
            print("STRING NOT FOUND!!")
            f.close(); return(1)
        f.close()
    print("ALL STRINGS FOUND."); return(0)

################################################################################
##============================================================================##
##------------------------------------ MAIN ----------------------------------##
##============================================================================##
################################################################################

if (len(sys.argv) <> 4):
    print "Syntax: python runtest.py <mpi-exe> <exe-root> <test-name>"
    sys.exit(1)

mpiexe = str(sys.argv[1])
exeroot = str(sys.argv[2])
testname = str(sys.argv[3])
stdoutfn = "std.out"

# Test if basic cmd-line options run as expected
if((testname=="helptest") or (testname=="infotest") or (testname=="inputest")):
    if(testname=="helptest"): flag = "--help"
    if(testname=="infotest"): flag = "--info"
    if(testname=="inputest"): flag = "--input"
    executable = exeroot+"/parsplice"
    with open(stdoutfn, "w") as f: subprocess.call([executable,flag], stdout=f)
    sys.exit(cmp_std(stdoutfn))

# Test if parsplice-md will run
if(testname=="mdtest"):
    executable = exeroot+"/parsplice-md"
    with open(stdoutfn, "w") as f:
        subprocess.call([mpiexe,"-np","2",executable], stdout=f)
    nomatch = find_strs(stdoutfn,["FINISHING, RANK=0","FINISHING, RANK=1"])
    subprocess.call(["mv",stdoutfn,(stdoutfn+".0")])
    subprocess.call(["mv","states","states.0"])
    subprocess.call(["mv","traj.out","traj.out.0"])
    sys.exit(nomatch)

print("TEST-NAME NOT RECOGNIZED!"); sys.exit(1)
