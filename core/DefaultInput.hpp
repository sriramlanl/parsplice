/*
Copyright (c) 2016, Los Alamos National Security, LLC
All rights reserved.
Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DefaultInput_h
#define DefaultInput_h

// Define all default values of XML options here.
// Refer to these definitions throughout code for consistancy.

#define INLOC_DEF "./input/ps-config.xml" // XML Input file location
#define DBROOT_DEF "./"                   // DB root directory location
#define NTRAJ_DEF 1                       // Num official trajectories
#define USESL_DEF 0                       // Use SLParSplice
#define NSL_DEF 10.0                      // Num Sublattice domains in each dir
#define DSL_DEF 10.0                      // Sublattice buffer, fixed, and vacuum dist
#define NCOL_DEF 1                        // Num colors in SL
#define SLCYCLE_DEF 1                     // SL MinPerCycle default
#define SEGBLOCKS_DEF 1                   // Approx. num of blocks per segment
#define WMIN_DEF 1e-12                    // ParSplice.MC.MinimumTrajectoryWeigth
#define VISDUR_DEF 100000000              // ParSplice.MC.DefaultVisitDuration
#define PREDGRAN_DEF 1                    // ParSplice.MC.PredictionGranularity
#define NWORKER_DEF 1                     // ParSplice.Topology.NWorkers
#define MXTASKMSG_DEF 1000000             // ParSplice.MaximumTaskMesgSize
#define VALTOUT_DEF 1000000               // ParSplice.Splicer.ValidatorTimeout
#define BCASTDELAY_DEF 1                  // ParSplice.Splicer.BroadcastDelay
#define RPRTDELAY_DEF 10000               // ParSplice.Splicer.ReportDelay
#define CHKDELAY_DEF 100000               // ParSplice.Splicer.CheckpointDelay
#define FLUSHONMOD_DEF 0                  // ParSplice.Splicer.FlushOnModify
#define RUNTIME_DEF 1000000               // ParSplice.RunTime
#define DISTSPEC_DEF ""                   // ParSplice.StateLabeler.DistinguishableSpecies
#define SMODDELAY_DEF 0                   // ParSplice.SystemModifier.Delay
#define STOPDUR_DEF 1000000               // ParSplice.StopDuration
#define VALBSIZE_DEF 1                    // ParSplice.ValidationBatchSize
#define DBSDELAY_DEF 1                    // ParSplice.DB.SyncDelay
#define OUTPUTVOL_DEF 1                   // ParSplice.DB.Splicer.OutputVolume
#define OUTPUTVOL_DEF 1                   // ParSplice.DB.Splicer.OutputVolume
#define USEKMC_DEF 0                      // ParSplice.MultiTrajectories.useKmcPredict
#define TRAJSLWAIT_DEF 1                  // ParSplice.MultiTrajectories.trajSLwait
#define RUNPYSCRIPT_DEF 0                 // ParSplice.MultiTrajectories.runPyScript
#define CHECK1ST_DEF 0                    // ParSplice.MultiTrajectories.check1st

#endif /* DefaultInput_h */
