/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#ifndef Graph_hpp
#define Graph_hpp

#include <iostream>
#include <stdio.h>
#include <map>
#include <boost/graph/iteration_macros.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include "AbstractSystem.hpp"
#include "NeighborList.hpp"
#include "Hash.hpp"
#include "DefaultInput.hpp"

#include <Eigen/Dense>


//using namespace boost;

extern "C"
{
#include "nauty/traces.h"
#include "nauty/nausparse.h"
}


typedef std::list<std::pair< std::map<int,int>, std::map<int,int> > >  StateToStateMappings;

typedef std::pair< std::map<int,int>, std::map<int,int> > StateToStateMapping;



struct vertex_p {
	vertex_p(){
		specie=0;
		uniqueIndex=-1;
		consecutiveIndex=-1;
		color=0;
	};
	int specie;
	int uniqueIndex;
	int consecutiveIndex;
	int color;
};

struct edge_p {
	edge_p(){
		type=0;
	};
	int type;
};

struct Distance {
	int i;
	double d;

	bool operator<(Distance other) const
	{
		return d < other.d;
	}

};





struct Interval {
	int i;
	int j;
	double di;
	double dj;
	double dd;
	double score;
	double score1;
	double score2;
	bool operator<(Interval other) const
	{
		return dd < other.dd;
	}
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, vertex_p,edge_p> graph_type;

template <class System> class AbstractStateLabeler {
public:
AbstractStateLabeler(boost::property_tree::ptree &config){
};
virtual uint64_t hash(System &s, bool canonical)=0;

void canonicalMap(System &s, std::map<int,int> &map, uint64_t &label){
	map.clear();
};

};


template <class System> class KMCStateLabeler : public AbstractStateLabeler<System> {
public:
KMCStateLabeler(boost::property_tree::ptree &config) : AbstractStateLabeler<System>(config) {
};
virtual uint64_t hash(System &s, bool canonical){
	return s.label;
};
};






template <class System> class VoronoiStateLabeler : public AbstractStateLabeler<System> {
public:
VoronoiStateLabeler(boost::property_tree::ptree &config) : AbstractStateLabeler<System>(config) {
	BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.StateLabeler.Bonds")) {
		std::string s=v.second.get<std::string>("Between");
		boost::trim(s);
		std::vector<std::string> sp;
		boost::split(sp, s, boost::is_any_of("\t "), boost::token_compress_on);
		int i0=boost::lexical_cast<int>(sp[0]);
		int i1=boost::lexical_cast<int>(sp[1]);
		double cut=v.second.get<double>("Cutoff");

		cutoffs[std::make_pair(i0,i1)]=cut;
		cutoffs[std::make_pair(i1,i0)]=cut;
	}

	//Initialize the reference!!!!

};

virtual uint64_t hash(System &s, bool canonical){

	NeighborList nList;
	MinimalSystem *system=dynamic_cast<MinimalSystem*>(&s);
	nList.buildJoint(reference,*system,cutoffs);

	std::map<int,std::multiset<int> > occupations;
	for(int i=0; i<s.getNAtoms(); i++) {
		double minDr=1e10;
		int imin=i;
		std::vector<double> positions(NDIM);
		for(int kk=0; kk<NDIM; kk++) {
			positions[kk]=s.getPosition(i,kk);
		}
		for(int j=0; j<nList.getNumberOfNeighbors(i); j++) {
			int ij=nList.getNeighbor(i,j);
			double dr=nearestImageDistance(positions, ij, reference, bc);
			if(dr<minDr) {
				minDr=dr;

				imin=j;
			}
		}
		//atom i is in the Voronoi volume of reference atom imin
		occupations[imin].insert(s.getSpecies(i));
	};
	boost::hash<std::map<int,std::multiset<int> > > stateHash;
	std::size_t h = stateHash(occupations);
	return fmix64(h);
};


private:
std::map<std::pair<int,int>,double> cutoffs;
System reference;
Cell bc;
};





template <class System> class ConnectivityGraphStateLabeler : public AbstractStateLabeler<System> {
public:
std::map<std::pair<int,int>,double> cutoffs;
std::map<std::pair<int,int>,double> nlcutoffs;
std::set<int> distinguishableSpecies;
bool canonical;
double skin;
double cut;

ConnectivityGraphStateLabeler(boost::property_tree::ptree &config) : AbstractStateLabeler<System>(config) {

	std::string ts=config.get<std::string>("ParSplice.StateLabeler.DistinguishableSpecies",DISTSPEC_DEF);
	boost::trim(ts);
	if(ts.size()>0) {
		//std::cout<<ts<<std::endl;
		std::vector<std::string> strs;
		boost::split(strs,ts,boost::is_any_of(","));
		for(auto it=strs.begin(); it!=strs.end(); it++) {
			//std::cout<<*it<<std::endl;
			std::string s=*it;
			boost::trim(s);
			int sp=boost::lexical_cast<int>(s);

			distinguishableSpecies.insert(sp);
			//std::cout<<"SPECIE "<<sp<<" IS DISTINGUISHABLE"<<std::endl;
		}
	}





	//cut=config.get<double>("ParSplice.StateLabeler.Cutoff");



	BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.StateLabeler.Bonds")) {
		std::string s=v.second.get<std::string>("Between");
		boost::trim(s);
		std::vector<std::string> sp;
		boost::split(sp, s, boost::is_any_of("\t "), boost::token_compress_on);
		int i0=boost::lexical_cast<int>(sp[0]);
		int i1=boost::lexical_cast<int>(sp[1]);
		double cut=v.second.get<double>("Cutoff");

		cutoffs[std::make_pair(i0,i1)]=cut;
		cutoffs[std::make_pair(i1,i0)]=cut;

		nlcutoffs[std::make_pair(i0,i1)]=cut+skin;
		nlcutoffs[std::make_pair(i1,i0)]=cut+skin;
	}




};

virtual uint64_t hash(System &s, bool canonical){


	graph_type g;
	buildGraph(s, cutoffs, g);

	std::map<int,int> map;
	uint64_t hash=hashGraph(g, canonical, map);
	//std::cout<<"LABELING "<<hash<<std::endl;
	return hash;
};

#if 1
void buildGraph(System &system,std::map<std::pair<int,int>,double> &cutoffs, graph_type &graph ){

	NeighborList nList;
	MinimalSystem *s=dynamic_cast<MinimalSystem*>(&system);
	nList.build(*s,cutoffs);

	//nList.build(system,cutoffs);

	graph.clear();

	boost::graph_traits<graph_type>::vertex_descriptor v;
	boost::graph_traits<graph_type>::vertex_descriptor u;
	boost::vertex_bundle_type<graph_type>::type vp;
	boost::edge_bundle_type<graph_type>::type ep;
	std::map<int, boost::graph_traits<graph_type>::vertex_descriptor > vertexMap;

	int nAtoms=system.getNAtoms();

	for(int i=0; i<nAtoms; i++) {
		vp.consecutiveIndex=i;
		vp.uniqueIndex=system.getUniqueID(i);
		vp.specie=system.getSpecies(i);
		int color;
		if(distinguishableSpecies.count(vp.specie)>0) {
			color=(vp.uniqueIndex+nAtoms+666);
		}
		else{
			color=vp.specie;
		}

		vp.color=color;
		v=add_vertex(vp,graph);
		vertexMap[i]=v;
	}


	for(int i=0; i<nAtoms; i++) {
		for(int j=0; j<nList.getNumberOfNeighbors(i); j++) {
			int ij=nList.getNeighbor(i,j);
			if(i>ij) {
				ep.type=0;
				v=vertexMap[i];
				u=vertexMap[ij];
				add_edge(v,u,ep,graph);
			}
		}
	}

	//std::cout<<"BUILDING GRAPH "<<num_vertices(graph)<<" "<<num_edges(graph)<<std::endl;

};

 #endif

#if 0
void buildGraph(System &system,std::map<std::pair<int,int>,double> &cutoffs, graph_type &graph ){

	int nAtoms=system.getNAtoms();

	//create the cutoff map
	nlcutoffs.clear();
	std::set<int> species;
	for(int i=0; i<nAtoms; i++) {
		species.insert(system.getSpecies(i));
	}
	for(auto it=species.begin(); it!=species.end(); it++) {
		for(auto it2=species.begin(); it2!=species.end(); it2++) {
			nlcutoffs[std::make_pair(*it,*it2)]=cut;
		}
	}

	NeighborList nList;
	MinimalSystem *s=dynamic_cast<MinimalSystem*>(&system);
	nList.build(*s,nlcutoffs);

	//nList.build(system,cutoffs);
	Eigen::MatrixXd rspe(NDIM,NDIM);
	Eigen::MatrixXd cspe(NDIM,NDIM);
	Cell bc;
	for(int i=0; i<NDIM; i++) {
		bc.periodic[i]=system.getPeriodic(i);
		for(int j=0; j<NDIM; j++) {
			bc.rsp[i][j]=system.getBox(i,j);
			rspe(i,j)=system.getBox(i,j);
		}
	}
	cspe=rspe.inverse();
	for(int i=0; i<NDIM; i++) {
		for(int j=0; j<NDIM; j++) {
			bc.csp[i][j]=cspe(i,j);
		}
	}


	graph.clear();

	boost::graph_traits<graph_type>::vertex_descriptor v;
	boost::graph_traits<graph_type>::vertex_descriptor u;
	boost::vertex_bundle_type<graph_type>::type vp;
	boost::edge_bundle_type<graph_type>::type ep;
	std::map<int, boost::graph_traits<graph_type>::vertex_descriptor > vertexMap;


	double epsilon=1e-0;

	//create the vertices
	for(int i=0; i<nAtoms; i++) {
		vp.consecutiveIndex=i;
		vp.uniqueIndex=system.getUniqueID(i);
		vp.specie=system.getSpecies(i);
		int color;
		if(distinguishableSpecies.count(vp.specie)>0) {
			color=(vp.uniqueIndex+nAtoms+666);
		}
		else{
			color=vp.specie;
		}

		vp.color=color;
		v=add_vertex(vp,graph);
		vertexMap[i]=v;
	}




	double ddelta=1e10;
	double maxcut=0;
	double mincut=1e10;
	//create the edges
	for(int i=0; i<nAtoms; i++) {

		std::set<Distance> distances;
		std::set<Interval> intervals;
		//compute all distances up to cut
		for(int j=0; j<nList.getNumberOfNeighbors(i); j++) {
			int ij=nList.getNeighbor(i,j);
			Distance d;
			d.i=ij;
			d.d=nearestImageDistance(i,ij,system,bc);
			distances.insert(d);
		}

		//create intervals in the space of distances
		double s1=0;
		double s2=0;
		double m1=-1e300;
		double m2=-1e300;

		for(auto it=distances.begin(); it!=distances.end(); it++ ) {
			auto itn=std::next(it, 1);

			if(itn!=distances.end()) {
				Interval interval;
				interval.i=it->i;
				interval.j=itn->i;
				interval.di=it->d;
				interval.dj=itn->d;
				interval.dd=itn->d-it->d;

				//double rc=(interval.dj+interval.di)/2;
				interval.score1=interval.dd/epsilon;
				m1=std::max(m1,interval.score1);

				intervals.insert(interval);
			}
			else{
				break;
			}
		}


		for(auto it=intervals.begin(); it!=intervals.end(); it++) {
			Interval &interval = const_cast<Interval&>(*it);
			interval.score1=exp( interval.score1-m1);

			double rc=(interval.dj+interval.di)/2;
			interval.score2=(interval.score1/rc/epsilon);
			m2=std::max(m2,interval.score2);
		}


		for(auto it=intervals.begin(); it!=intervals.end(); it++) {
			Interval &interval = const_cast<Interval&>(*it);
			interval.score2=exp(interval.score2-m2);
		}


		double maxr=0;
		Interval intvmax;
		double maxScore=0;
		for(auto it=intervals.begin(); it!=intervals.end(); it++) {
			Interval &interval = const_cast<Interval&>(*it);
			//interval.score=(interval.score1)*(1+(interval.score2))/2.;
			interval.score=interval.score2;
			if(interval.score>maxScore) {
				maxScore=interval.score;
				intvmax=interval;
			}
		}



		std::cout<<"dd "<<i<<" "<<intvmax.i<<" "<<intvmax.j<<" "<<intvmax.dd<<" "<<intvmax.score1<<" "<<intvmax.score2<<" "<<intvmax.score<<std::endl;


		auto it=intervals.rbegin();

		std::cout<<"ddm "<<i<<" "<<it->i<<" "<<it->j<<" "<<it->dd<<" "<<it->score1<<" "<<it->score2<<" "<<it->score<<std::endl;
		it++;
		std::cout<<"ddmm "<<i<<" "<<it->i<<" "<<it->j<<" "<<it->dd<<" "<<it->score1<<" "<<it->score2<<" "<<it->score<<std::endl;



		//find the most robust interval
		/*
		   for(auto it=intervals.begin(); it!=intervals.end(); it++) {

		        double r=it->dd;

		           if(it!=intervals.begin()) {
		                auto itp=std::prev(it, 1);
		                r=std::min(r, fabs(itp->dd-it->dd));
		           }
		           auto itn=std::next(it, 1);
		           if(itn!=intervals.end()) {
		                r=std::min(r, fabs(itn->dd-it->dd));
		           }


		        if(r>maxr) {
		                intvmax=*it;
		                maxr=r;
		        }
		   }
		 */


		/*
		   if(i==6657) {
		        std::cout<<"MAXR: "<<maxr<<std::endl;
		   }
		 */

		/*
		   if(intervals.size()>0) {
		       intvmax=*intervals.rbegin();
		   }
		   if(intervals.size()>1) {
		       auto it=intervals.rbegin();
		       it++;
		       maxr=intvmax.dd-it->dd;

		       if(maxr<1e-3) {

		       }
		   }
		   else{
		       maxr=1;
		   }

		   ddelta=std::min(ddelta,maxr);
		 */

		/*
		   std::cout<<"========================="<<std::endl;
		   for(auto itd=distances.begin(); itd!=distances.end(); itd++) {
		       std::cout<<"d "<<i<<" "<<itd->i<<" "<<itd->d<<std::endl;
		   }

		   for(auto itd=intervals.begin(); itd!=intervals.end(); itd++) {
		       std::cout<<"d "<<i<<" "<<itd->i<<" "<<itd->j<<" "<<itd->dd<<" "<<itd->score1<<" "<<itd->score2<<" "<<itd->score<<std::endl;
		   }
		 */


		double rcut=0;
		if(intervals.size()>0) {
			rcut=(intvmax.dj+intvmax.di)/2;
		}

		/*
		   if(i==6657) {
		        std::cout<<"RCUT "<<rcut<<std::endl;
		   }
		 */
		maxcut=std::max(rcut,maxcut);
		mincut=std::min(rcut,mincut);

		//find neighbors
		for(auto it=distances.begin(); it!=distances.end(); it++ ) {
			if(it->d<=rcut) {
				if(i>it->i) {
					ep.type=0;
					v=vertexMap[i];
					u=vertexMap[it->i];
					add_edge(v,u,ep,graph);
					/*
					   if(i==6657) {
					        std::cout<<"* "<<i<<" "<<it->i<<std::endl;
					   }
					 */
				}
			}
		}
	}








	std::cout<<"BUILDING GRAPH "<<num_vertices(graph)<<" "<<num_edges(graph)<<std::endl;
	std::cout<<"DELTA: "<<ddelta<<" "<<mincut<<" "<<maxcut<<std::endl;

};

#endif




#if 0


void buildGraph(System &system,std::map<std::pair<int,int>,double> &cutoffs, graph_type &graph ){

	int nAtoms=system.getNAtoms();

	//create the cutoff map
	nlcutoffs.clear();
	std::set<int> species;
	for(int i=0; i<nAtoms; i++) {
		species.insert(system.getSpecies(i));
	}
	for(auto it=species.begin(); it!=species.end(); it++) {
		for(auto it2=species.begin(); it2!=species.end(); it2++) {
			nlcutoffs[std::make_pair(*it,*it2)]=cut;
		}
	}

	NeighborList nList;
	MinimalSystem *s=dynamic_cast<MinimalSystem*>(&system);
	nList.build(*s,nlcutoffs);

	//nList.build(system,cutoffs);
	Eigen::MatrixXd rspe(NDIM,NDIM);
	Eigen::MatrixXd cspe(NDIM,NDIM);
	Cell bc;
	for(int i=0; i<NDIM; i++) {
		bc.periodic[i]=system.getPeriodic(i);
		for(int j=0; j<NDIM; j++) {
			bc.rsp[i][j]=system.getBox(i,j);
			rspe(i,j)=system.getBox(i,j);
		}
	}
	cspe=rspe.inverse();
	for(int i=0; i<NDIM; i++) {
		for(int j=0; j<NDIM; j++) {
			bc.csp[i][j]=cspe(i,j);
		}
	}


	graph.clear();

	boost::graph_traits<graph_type>::vertex_descriptor v;
	boost::graph_traits<graph_type>::vertex_descriptor u;
	boost::vertex_bundle_type<graph_type>::type vp;
	boost::edge_bundle_type<graph_type>::type ep;
	std::map<int, boost::graph_traits<graph_type>::vertex_descriptor > vertexMap;


	//create the vertices
	for(int i=0; i<nAtoms; i++) {
		vp.consecutiveIndex=i;
		vp.uniqueIndex=system.getUniqueID(i);
		vp.specie=system.getSpecies(i);
		int color;
		if(distinguishableSpecies.count(vp.specie)>0) {
			color=(vp.uniqueIndex+nAtoms+666);
		}
		else{
			color=vp.specie;
		}

		vp.color=color;
		v=add_vertex(vp,graph);
		vertexMap[i]=v;
	}




	double ddelta=1e10;
	double maxcut=0;
	double mincut=1e10;

	std::map<int, std::set<int> > nn;

	for(int i=0; i<nAtoms; i++) {
		for(int ii=0; ii<nList.getNumberOfNeighbors(i); ii++) {
			nn[i].insert(nList.getNeighbor(i,ii));
		}
	}

	//create the edges
	for(int i=0; i<nAtoms; i++) {

		for(auto ni=nn[i].begin(); ni!=nn[i].end(); ni++) {
			int j=*ni;
			//consider the i-j bond
			if(i<j) {
				std::set<int> cn=nn[i];
				cn.insert(nn[j].begin(), nn[j].end());

			}
		}


		for(int ii=0; ii<nList.getNumberOfNeighbors(i); ii++) {
			int j=nList.getNeighbor(i,ii);
			if(i<j) {



			}
		}

		std::set<Distance> distances;
		std::set<Interval> intervals;
		//compute all distances up to cut
		for(int j=0; j<nList.getNumberOfNeighbors(i); j++) {
			int ij=nList.getNeighbor(i,j);
			Distance d;
			d.i=ij;
			d.d=nearestImageDistance(i,ij,system,bc);
			distances.insert(d);

			/*
			   if(i==6657) {
			        std::cout<<"d "<<i<<" "<<ij<<" "<<d.d<<std::endl;
			   }
			 */
		}

		//create intervals in the space of distances
		for(auto it=distances.begin(); it!=distances.end(); it++ ) {
			auto itn=std::next(it, 1);

			if(itn!=distances.end()) {
				Interval interval;
				interval.i=it->i;
				interval.j=itn->i;
				interval.di=it->d;
				interval.dj=itn->d;
				interval.dd=itn->d-it->d;
				intervals.insert(interval);
			}
			else{
				break;
			}
		}



		double maxr=0;
		Interval intvmax;
		//find the most robust interval
		/*
		   for(auto it=intervals.begin(); it!=intervals.end(); it++) {

		        double r=it->dd;

		           if(it!=intervals.begin()) {
		                auto itp=std::prev(it, 1);
		                r=std::min(r, fabs(itp->dd-it->dd));
		           }
		           auto itn=std::next(it, 1);
		           if(itn!=intervals.end()) {
		                r=std::min(r, fabs(itn->dd-it->dd));
		           }


		        if(r>maxr) {
		                intvmax=*it;
		                maxr=r;
		        }
		   }
		 */


		/*
		   if(i==6657) {
		        std::cout<<"MAXR: "<<maxr<<std::endl;
		   }
		 */

		if(intervals.size()>0) {
			intvmax=*intervals.rbegin();
		}
		if(intervals.size()>1) {
			auto it=intervals.rbegin();
			it++;
			maxr=intvmax.dd-it->dd;

			if(maxr<1e-3) {
				for(auto itd=distances.begin(); itd!=distances.end(); itd++) {
					std::cout<<"d "<<i<<" "<<itd->i<<" "<<itd->d<<std::endl;
				}
				for(auto itd=intervals.begin(); itd!=intervals.end(); itd++) {
					std::cout<<"d "<<i<<" "<<itd->i<<" "<<itd->j<<" "<<itd->dd<<std::endl;
				}
			}
		}
		else{
			maxr=1;
		}

		ddelta=std::min(ddelta,maxr);


		double rcut=0;
		if(intervals.size()>0) {
			rcut=(intvmax.dj+intvmax.di)/2;
		}

		/*
		   if(i==6657) {
		        std::cout<<"RCUT "<<rcut<<std::endl;
		   }
		 */
		maxcut=std::max(rcut,maxcut);
		mincut=std::min(rcut,mincut);

		//find neighbors
		for(auto it=distances.begin(); it!=distances.end(); it++ ) {
			if(it->d<=rcut) {
				if(i>it->i) {
					ep.type=0;
					v=vertexMap[i];
					u=vertexMap[it->i];
					add_edge(v,u,ep,graph);
					/*
					   if(i==6657) {
					        std::cout<<"* "<<i<<" "<<it->i<<std::endl;
					   }
					 */
				}
			}
		}
	}








	std::cout<<"BUILDING GRAPH "<<num_vertices(graph)<<" "<<num_edges(graph)<<std::endl;
	std::cout<<"DELTA: "<<ddelta<<" "<<mincut<<" "<<maxcut<<std::endl;

};

#endif


std::map<int,int> canonicalSort(graph_type &ga){
	std::map<int,int> canonicalMap;


	//map boost iterator order to consecutive order
	std::map<int, int > vertexMap;
	std::map<int, int > vertexMapUnique;
	std::map<int, int > ivertexMap;

	std::map<int, std::list<int> > colorMap;
	int i=0;
	auto vi= boost::vertices(ga);
	for(auto itv=vi.first; itv!=vi.second; itv++) {
		//vertex color
		int color=ga[*itv].color;
		colorMap[color].push_back(i);
		//index map
		vertexMap[i]=ga[*itv].consecutiveIndex;
		vertexMapUnique[i]=ga[*itv].uniqueIndex;
		ivertexMap[ga[*itv].consecutiveIndex]=i;
		i++;
	}

	unsigned long nVerts = num_vertices(ga);
	unsigned long nEdges=num_edges(ga);



	//convert to TRACES format
	static DEFAULTOPTIONS_TRACES(options);
	TracesStats stats;
	SG_DECL(sg1);
	SG_DECL(cg1);
	DYNALLSTAT(int,lab1,lab1_sz);
	DYNALLSTAT(int,ptn,ptn_sz);
	DYNALLSTAT(int,orbits,orbits_sz);

	options.getcanon = true;
	options.defaultptn = false;

	SG_ALLOC(sg1,nVerts,2*nEdges,"malloc");

	DYNALLOC1(int,lab1,lab1_sz,nVerts,"malloc");
	DYNALLOC1(int,ptn,ptn_sz,nVerts,"malloc");
	DYNALLOC1(int,orbits,orbits_sz,nVerts,"malloc");

	sg1.nv = int(nVerts); /* Number of vertices */
	sg1.nde = int(2*nEdges); /* Number of directed edges */

//colors
	i=0;
	for(auto itc=colorMap.begin(); itc!=colorMap.end(); itc++) {
		for(auto ita=itc->second.begin(); ita!=itc->second.end(); ita++) {
			lab1[i]=*ita;
			ptn[i]=1;
			i++;
		}
		ptn[i-1]=0;
	}

	i=0;
	int ie=0;
//create the edges in traces ordering

//std::cout<<"BONDS IN TRACE INDEX"<<std::endl;
	for(auto itv=vi.first; itv!=vi.second; itv++) {

		unsigned long na=boost::out_degree(*itv, ga);
		sg1.d[i]=int(na);
		sg1.v[i]=ie;


		auto va=boost::adjacent_vertices(*itv,ga);
		int j=0;
		for(auto itva=va.first; itva!=va.second; itva++) {
			sg1.e[sg1.v[i]+j]=ivertexMap[ga[*itva].consecutiveIndex];
			//std::cout<<i<<" <-> "<<ivertexMap[ga[*itva].consecutiveIndex]<<std::endl;
			j++;
		}
		ie+=j;
		i++;
	}

//call traces
	Traces(&sg1,lab1,ptn,orbits,&options,&stats,&cg1);



//convert to map unique indices
	for(int i=0; i<nVerts; i++) {
		int tracesIndex=i+1;
		int uniqueIndex=vertexMapUnique[tracesIndex];

		int remappedTraceIndex=lab1[i];
		int remappedUniqueIndex=vertexMapUnique[remappedTraceIndex];

		canonicalMap[remappedUniqueIndex]=tracesIndex;
	}

//free traces memory
	SG_FREE(sg1);
	SG_FREE(cg1);
	DYNFREE(lab1,lab1_sz);
	DYNFREE(ptn,ptn_sz);
	DYNFREE(orbits,orbits_sz);


	return canonicalMap;
};

/*
   void canonicalSort(System &s){
        graph_type g;
        buildGraph(s, cutoffs, g);
        std::map<int,int> map;
        s.label=hashGraph(g,true,map);
        s.remap(map);
   };
 */

void canonicalMap(System &s, std::map<int,int> &map, uint64_t &label){
	graph_type g;
	buildGraph(s, cutoffs, g);
	map.clear();
	label=hashGraph(g,true,map);
};


uint64_t hashGraph(graph_type &ga, bool canonical, std::map<int,int> &canonicalMap){
	canonicalMap.clear();

	//find the canonical labeling if necessary
	if(canonical) {
		canonicalMap=canonicalSort(ga);
		/*
		   for(auto it=canonicalMap.begin(); it!=canonicalMap.end(); it++) {
		        std::cout<<it->first<<" -> "<<it->second<<std::endl;
		   }
		 */
	}
	else{
		//identity mapping
		auto vi= boost::vertices(ga);
		for(auto itv=vi.first; itv!=vi.second; itv++) {
			int uniqueIndex=ga[*itv].uniqueIndex;
			int remappedUniqueIndex=uniqueIndex;
			canonicalMap[uniqueIndex]=remappedUniqueIndex;
		}
	}


	//hash
	uint64_t hash=0;


	//hash the vertices
	auto vi= boost::vertices(ga);
	for(auto itv=vi.first; itv!=vi.second; itv++) {

		//create a 64 bits vertex index with the unique index and the color

		uint32_t i=uint32_t(canonicalMap[ga[*itv].uniqueIndex]);
		uint32_t c=uint32_t(ga[*itv].color);
		i=fmix32(i);
		c=fmix32(c);

		uint64_t l= i + (uint64_t(c) << 32);

		//hash it
		uint64_t lh=fmix64(l);
		//add it to the hash
		hash = hash ^ lh;
	}
	//std::cout<<"VERTEX HASH "<<hash<<std::endl;

	//hash the edges
	BGL_FORALL_EDGES_T(ed, ga, graph_type)
	{
		unsigned long v1, v2;
		v1 = source(ed,ga);
		v2 = target(ed,ga);


		uint32_t unique1=uint32_t(canonicalMap[ga[v1].uniqueIndex]);
		uint32_t unique2=uint32_t(canonicalMap[ga[v2].uniqueIndex]);

		unique1=fmix32(unique1);
		unique2=fmix32(unique2);

		//create a 64 bits edge index with the two vertices
		uint64_t min=uint64_t(std::min(unique1,unique2));
		uint64_t max=uint64_t(std::max(unique1,unique2));
		uint64_t l= max + (min << 32);


		//hash it
		uint64_t lh=fmix64(l);
		//add it to the hash
		hash = hash ^ lh;
		//std::cout<<l<<" "<<lh<<" "<<hash<<std::endl;
	}

	//std::cout<<"VERTEX + EDGE HASH "<<hash<<std::endl;

	return hash;

};
};




#endif /* Graph_hpp */
